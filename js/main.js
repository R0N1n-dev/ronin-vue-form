new Vue({
    el: "#app",
    data() {
        return {
            fields: {
                name: "",
                arrivalDate: "",
                departureDate: "",
                reasons: "",
                services: "",
                comments: "",
            },
            fieldErrors: {
                name: undefined,
                arrivalDate: undefined,
                departureDate: undefined,
                reasons: undefined,
                services: undefined,
                comments: undefined,
            },
            reviews: [],
        };
    },
    computed: {
        isNameLimitExceeded() {
            return this.fields.name.length >= 20;
        },
        isCommentLimitExceeded() {
            return this.fields.comments.length >= 300;
        },
    },

    methods: {
        submitForm: function(e) {
            e.preventDefault();
            this.fieldErrors = this.validateForm(this.fields);
            if (Object.keys(this.fieldErrors).length) {
                anime({
                    targets: "form.form", //target of animation
                    translateY: [
                        { value: 30, duration: 100 },
                        { value: 0, duration: 100 },
                    ],
                });
                return;
            }

            this.reviews.push(this.comments);
            this.fields.name = "";
            this.fields.arrivalDate = "";
            this.fields.departureDate = "";
            this.fields.reasons = "";
            this.fields.services = "";
            this.fields.comments = "";
        },
        validateForm(fields) {
            const errors = {};
            if (!fields.name) {
                errors.name = "At least a single name required";
            }
            if (!fields.arrivalDate) {
                errors.arrivalDate = "Date of arrival required";
            }
            if (!fields.departureDate) {
                errors.departureDate = "Date of departure required";
            }
            if (!fields.reasons) {
                errors.reasons = "Reason for visit required";
            }
            if (!fields.services) {
                errors.services = "Please select services used during visit";
            }
            if (!fields.comments) {
                errors.comments = "Please leave your comments";
            }
            return errors;
        },
    },
});